﻿var jDashboard = {
    vars: {
        endpoint:  '/',
     
    },
    init: function () {
        //$('#btnSearch').click(function () {
        //    adminList.getData(1);
        //});
       // jDashboard.stackLineChar = echart.init(jDashboard.Chart.option)
    },
    Chart: {
        htmlTag: {
            formId: '#frmDataList',
            targetId: '#stackLineChart',
            stackLineChar: null
        },

        init: function () {
            // jDashboard.List.bindData();
            if (jDashboard.Chart.option) {
                jDashboard.Chart.stackLineChar = echarts.init($(jDashboard.Chart.htmlTag.targetId)[0]);
                jDashboard.Chart.stackLineChar.setOption(jDashboard.Chart.option);
            }
        },

        option: {
            title: {
                text: 'Thống kê doanh thu'
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['Email', 'Union Ads', 'Video Ads', 'Direct', 'Search Engine']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['08-04', '09-04', '10-04', '11-04', '12-04', '13-04', '14-04']
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: 'Email',
                    type: 'line',
                    stack: 'Total',
                    data: [120, 132, 101, 134, 90, 230, 210]
                },
                {
                    name: 'Union Ads',
                    type: 'line',
                    stack: 'Total',
                    data: [220, 182, 191, 234, 290, 330, 310]
                },
                {
                    name: 'Video Ads',
                    type: 'line',
                    stack: 'Total',
                    data: [150, 232, 201, 154, 190, 330, 410]
                },
                {
                    name: 'Direct',
                    type: 'line',
                    stack: 'Total',
                    data: [320, 332, 301, 334, 390, 330, 320]
                },
                {
                    name: 'Search Engine',
                    type: 'line',
                    stack: 'Total',
                    data: [820, 932, 901, 934, 1290, 1330, 1320]
                }
            ]
        }

    },

}
