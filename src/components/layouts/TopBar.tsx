import React from "react";

export const TopBar = () => {
  return (
    <div className="top-bar -mx-4 px-4 md:mx-0 md:px-0">
      <nav aria-label="breadcrumb" className="-intro-x mr-auto hidden sm:flex">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <a href="#">Cấu hình</a>
          </li>
          <li className="breadcrumb-item active" aria-current="page">
            Người dùng
          </li>
        </ol>
      </nav>
      <div className="intro-x relative mr-3 sm:mr-6">
        <div className="search hidden sm:block">
          <input
            type="text"
            className="search__input form-control border-transparent"
            placeholder="Tìm kiếm..."
          />
          <i
            data-lucide="search"
            className="search__icon dark:text-slate-500"
          ></i>
        </div>
        <a className="notification sm:hidden" href="">
          <i
            data-lucide="search"
            className="notification__icon dark:text-slate-500"
          ></i>
        </a>
      </div>
      <div className="intro-x dropdown mr-auto sm:mr-6">
        <div
          className="dropdown-toggle notification notification--bullet cursor-pointer"
          role="button"
          aria-expanded="false"
          data-tw-toggle="dropdown"
        >
          <i
            data-lucide="bell"
            className="notification__icon dark:text-slate-500"
          ></i>
        </div>
        <div className="notification-content pt-2 dropdown-menu">
          <div className="notification-content__box dropdown-content">
            <div className="notification-content__title">Notifications</div>
            <div className="cursor-pointer relative flex items-center ">
              <div className="w-12 h-12 flex-none image-fit mr-1">
                <img
                  alt="Midone - HTML Admin Template"
                  className="rounded-full"
                  src="dist/images/profile-4.jpg"
                />
                <div className="w-3 h-3 bg-success absolute right-0 bottom-0 rounded-full border-2 border-white dark:border-darkmode-600"></div>
              </div>
              <div className="ml-2 overflow-hidden">
                <div className="flex items-center">
                  <a href="javascript:;" className="font-medium truncate mr-5">
                    Denzel Washington
                  </a>
                  <div className="text-xs text-slate-400 ml-auto whitespace-nowrap">
                    01:10 PM
                  </div>
                </div>
                <div className="w-full truncate text-slate-500 mt-0.5">
                  Contrary to popular belief, Lorem Ipsum is not simply random
                  text. It has roots in a piece of classical Latin literature
                  from 45 BC, making it over 20
                </div>
              </div>
            </div>
            <div className="cursor-pointer relative flex items-center mt-5">
              <div className="w-12 h-12 flex-none image-fit mr-1">
                <img
                  alt="Midone - HTML Admin Template"
                  className="rounded-full"
                  src="dist/images/profile-5.jpg"
                />
                <div className="w-3 h-3 bg-success absolute right-0 bottom-0 rounded-full border-2 border-white dark:border-darkmode-600"></div>
              </div>
              <div className="ml-2 overflow-hidden">
                <div className="flex items-center">
                  <a href="javascript:;" className="font-medium truncate mr-5">
                    Nicolas Cage
                  </a>
                  <div className="text-xs text-slate-400 ml-auto whitespace-nowrap">
                    01:10 PM
                  </div>
                </div>
                <div className="w-full truncate text-slate-500 mt-0.5">
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem{" "}
                </div>
              </div>
            </div>
            <div className="cursor-pointer relative flex items-center mt-5">
              <div className="w-12 h-12 flex-none image-fit mr-1">
                <img
                  alt="Midone - HTML Admin Template"
                  className="rounded-full"
                  src="dist/images/profile-15.jpg"
                />
                <div className="w-3 h-3 bg-success absolute right-0 bottom-0 rounded-full border-2 border-white dark:border-darkmode-600"></div>
              </div>
              <div className="ml-2 overflow-hidden">
                <div className="flex items-center">
                  <a href="javascript:;" className="font-medium truncate mr-5">
                    Tom Cruise
                  </a>
                  <div className="text-xs text-slate-400 ml-auto whitespace-nowrap">
                    05:09 AM
                  </div>
                </div>
                <div className="w-full truncate text-slate-500 mt-0.5">
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem{" "}
                </div>
              </div>
            </div>
            <div className="cursor-pointer relative flex items-center mt-5">
              <div className="w-12 h-12 flex-none image-fit mr-1">
                <img
                  alt="Midone - HTML Admin Template"
                  className="rounded-full"
                  src="dist/images/profile-11.jpg"
                />
                <div className="w-3 h-3 bg-success absolute right-0 bottom-0 rounded-full border-2 border-white dark:border-darkmode-600"></div>
              </div>
              <div className="ml-2 overflow-hidden">
                <div className="flex items-center">
                  <a href="javascript:;" className="font-medium truncate mr-5">
                    Denzel Washington
                  </a>
                  <div className="text-xs text-slate-400 ml-auto whitespace-nowrap">
                    01:10 PM
                  </div>
                </div>
                <div className="w-full truncate text-slate-500 mt-0.5">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry&#039;s
                  standard dummy text ever since the 1500
                </div>
              </div>
            </div>
            <div className="cursor-pointer relative flex items-center mt-5">
              <div className="w-12 h-12 flex-none image-fit mr-1">
                <img
                  alt="Midone - HTML Admin Template"
                  className="rounded-full"
                  src="dist/images/profile-15.jpg"
                />
                <div className="w-3 h-3 bg-success absolute right-0 bottom-0 rounded-full border-2 border-white dark:border-darkmode-600"></div>
              </div>
              <div className="ml-2 overflow-hidden">
                <div className="flex items-center">
                  <a href="javascript:;" className="font-medium truncate mr-5">
                    Sylvester Stallone
                  </a>
                  <div className="text-xs text-slate-400 ml-auto whitespace-nowrap">
                    01:10 PM
                  </div>
                </div>
                <div className="w-full truncate text-slate-500 mt-0.5">
                  Contrary to popular belief, Lorem Ipsum is not simply random
                  text. It has roots in a piece of classical Latin literature
                  from 45 BC, making it over 20
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="intro-x dropdown w-8 h-8">
        <div
          className="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in"
          role="button"
          aria-expanded="false"
          data-tw-toggle="dropdown"
        >
          <img alt="Midone - HTML Admin Template" src="dist/images/avt.png" />
        </div>
        <div className="dropdown-menu w-56">
          <ul className="dropdown-content bg-primary text-white">
            <li className="p-2">
              <div className="font-medium">Admin</div>
              <div className="text-xs text-white/70 mt-0.5 dark:text-slate-500">
                Nhân viên CSKH
              </div>
            </li>
            <li>
              <hr className="dropdown-divider border-white/[0.08]" />
            </li>
            <li>
              <a href="" className="dropdown-item hover:bg-white/5">
                <i data-lucide="user" className="w-4 h-4 mr-2"></i> Hồ sơ cá
                nhân
              </a>
            </li>
            <li>
              <a href="" className="dropdown-item hover:bg-white/5">
                <i data-lucide="edit" className="w-4 h-4 mr-2"></i> Account
                Setting
              </a>
            </li>
            <li>
              <a
                href="javascript:;"
                data-tw-toggle="modal"
                data-tw-target="#header-footer-modal-preview"
                className="dropdown-item hover:bg-white/5"
              >
                <i data-lucide="lock" className="w-4 h-4 mr-2"></i> Đổi mật khẩu
              </a>
            </li>
            <li>
              <hr className="dropdown-divider border-white/[0.08]" />
            </li>
            <li>
              <a href="" className="dropdown-item hover:bg-white/5">
                <i data-lucide="toggle-right" className="w-4 h-4 mr-2"></i> Đăng
                xuất
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
