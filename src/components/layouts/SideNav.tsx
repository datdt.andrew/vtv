import React from "react";

export const SideNav = () => {
  return (
    <nav className="side-nav">
      <a className="intro-x flex items-center pl-5 pt-4 mt-3">
        <img
          alt="VTVLive - Nền tảng dữ liệu và trải nghiệm khách hàng"
          className=""
          src="dist/images/Logo-full.png"
        />
      </a>
      <div className="side-nav__devider my-6"></div>
      <ul>
        <li>
          <a className="side-menu side-menu--open">
            <div className="side-menu__icon">
              <i data-lucide="box"></i>
            </div>
            <div className="side-menu__title">
              Campaign
              <div className="side-menu__sub-icon ">
                <i data-lucide="chevron-down"></i>{" "}
              </div>
            </div>
          </a>
          <ul className="side-menu__sub-open">
            <li>
              <a className="side-menu">
                <div className="side-menu__icon">
                  <i data-lucide="activity"></i>
                </div>
                <div className="side-menu__title"> Quản lý chiến dịch </div>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
  );
};
