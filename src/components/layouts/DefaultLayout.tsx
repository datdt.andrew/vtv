import React from "react";
import { MobileMenu } from "./MobileMenu";
import { SideNav } from "./SideNav";
import { TopBar } from "./TopBar";

export const DefaultLayout = (props: any) => {
  const { children } = props;
  return (
    <>
      <body className="py-5 md:py-0 bg-black/[0.15] dark:bg-transparent">
        <MobileMenu />
        <div className="flex mt-[4.7rem] md:mt-0 overflow-hidden">
          <SideNav />
          <div className="content">
            <TopBar />
            <div>{children}</div>
          </div>
        </div>
      </body>
    </>
  );
};
