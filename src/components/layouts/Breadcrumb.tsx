import React from "react";

export const HeadTitle = (props: any) => {
  const { title, description } = props;
  return (
    <div className="intro-y items-center mt-5">
      <h2 className="text-lg font-medium mr-auto">{title}</h2>
      <p>{description}</p>
    </div>
  );
};
