import React from "react";
import { ReactNode } from "react";

export const Section = (props: SectionProps) => {
  const { title, extra = null, children } = props;
  return (
    <div className="flex flex-col gap-6">
      <div className="flex justify-between gap-4">
        <div className="text-xl font-semibold">{title}</div>
        {extra && <>{extra}</>}
      </div>
      <div>{children}</div>
    </div>
  );
};

export interface SectionTitleProps {
  title: string;
  extra?: any;
}

export const SectionTitle = (props: SectionTitleProps) => {
  const { title, extra } = props;
  return (
    <div className="flex items-center gap-4">
      <span>{title}</span>
      {extra}
    </div>
  );
};

export interface SectionProps {
  title: string | ReactNode;
  extra?: any;
  children?: any;
}
