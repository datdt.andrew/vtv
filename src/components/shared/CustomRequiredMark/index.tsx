import React from "react";

export const CustomizeRequiredMark = (
  label: React.ReactNode,
  { required }: { required: boolean }
) => (
  <>
    {label}
    {required && <span className="text-[#E10600] ml-1">*</span>}
  </>
);