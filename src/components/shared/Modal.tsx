export const Modal = () => {
  return (
    <div
      id="header-footer-modal-preview"
      className="modal"
      tabIndex={-1}
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <a data-tw-dismiss="modal" href="javascript:;">
            <i data-lucide="x" className="w-6 h-6 text-slate-400"></i>{" "}
          </a>
          <div className="modal-header">
            <h2 className="font-medium text-base mr-auto">Đổi mật khẩu</h2>
          </div>
          <div className="modal-body">
            <div className="intro-x mt-2">
              <div>
                <label className="form-label v-label-form">Mật khẩu mới</label>
                <input
                  id="crud-form-1"
                  type="Password"
                  className="form-control w-full v-input"
                  placeholder="Nhập mật khẩu mới"
                />
              </div>
              <div className="mt-3">
                <label className="form-label v-label-form ">
                  Nhập lại mật khẩu
                </label>
                <input
                  id="crud-form-1"
                  type="Password"
                  className="form-control w-full v-input"
                  placeholder="Nhập lại mật khẩu mới"
                />
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              data-tw-dismiss="modal"
              className="btn btn-outline-secondary w-20 mr-1"
            >
              Hủy
            </button>{" "}
            <button type="button" className="btn btn-primary w-20">
              Lưu lại
            </button>{" "}
          </div>
        </div>
      </div>
    </div>
  );
};
