import React, { FC } from "react";
import { Modal as AntModal, ConfigProvider } from "antd";
import { Divider } from "antd";

export interface ModalProps {
  isModalOpen: boolean;
  width?: number;
  className?: string;
  children?: React.ReactNode;
  closeIcon?: boolean | React.ReactNode;
  handleClose: () => void;
  title: React.ReactNode;
}

const Modal: FC<ModalProps> = ({
  isModalOpen,
  width,
  closeIcon = false,
  children,
  title,
  handleClose = () => {},
}) => {
  return (
    <ConfigProvider
      theme={{
        components: {
          Modal: {
            fontFamily: "Inter",
            titleColor: "#333",
          },
        },
      }}
    >
      <AntModal
        open={isModalOpen}
        width={width}
        closeIcon={closeIcon}
        onCancel={handleClose}
        footer={null}
        title={
          <div className="pt-5">
            <div className="text-center px-4">{title}</div>
            <Divider className="my-4" />
          </div>
        }
      >
        <div className="p-4 pt-0">{children}</div>
      </AntModal>
    </ConfigProvider>
  );
};

export default Modal;
