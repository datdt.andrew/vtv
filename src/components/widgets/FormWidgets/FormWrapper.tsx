import React from 'react'
import { FormActionButton } from './FormActionButton'

export interface FormWrapperProps {
  children: any
  onSubmit?: any
  onCancel?: any
  title?: string
  loading?: boolean
  cancelText?: string
  submitText?: string
}

export const FormWrapper = (props: FormWrapperProps) => {
  const {
    title,
    children,
    onSubmit,
    onCancel,
    loading,
    submitText,
    cancelText,
  } = props
  if (loading) return <>loading...</>
  return (
    <>
      <form onSubmit={onSubmit}>
        <div className="flex flex-col gap-4">
          {title && <div className="font-bold text-xl">{title}</div>}
          {children}
          <FormActionButton
            onCancel={onCancel}
            showCancel={onCancel != null}
            showSubmit={onSubmit != null}
            submitText={submitText}
            cancelText={cancelText}
          />
        </div>
      </form>
    </>
  )
}
