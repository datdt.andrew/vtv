import React from "react";
import { Button } from "../../core";

export interface FormActionButtonProps {
  onCancel?: any;
  showCancel?: boolean;
  showSubmit?: boolean;
  onSubmit?: any;
  position?: string;
  cancelText?: string;
  submitText?: string;
}
export const FormActionButton = (props: FormActionButtonProps) => {
  const {
    onCancel,
    showCancel = true,
    showSubmit = true,
    onSubmit,
    position = "end",
    cancelText = "Cancel",
    submitText = "Submit",
  } = props;
  return (
    <div className={`flex gap-4 justify-${position}`}>
      {showCancel && <Button onClick={onCancel}>{cancelText}</Button>}

      {showSubmit && (
        <>
          <Button
            type="primary"
            onClick={onSubmit}
            htmlType={!onSubmit ? "submit" : "button"}
          >
            {submitText}
          </Button>
        </>
      )}
    </div>
  );
};
