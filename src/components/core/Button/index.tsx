import { Button as AntButton, ButtonProps } from "antd";
import "./Button.css"
export const Button = (props: ButtonProps) => {
  return <AntButton {...props} />;
};
