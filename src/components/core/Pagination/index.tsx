import { Pagination as AntPagination } from "antd";
import React from "react";

export interface PaginationProps {
  totalElements?: number;
  number?: number;
  numberOfElements?: number;
  size?: number;
  onChange?: any;
}

export const Pagination = (props: PaginationProps) => {
  const {
    totalElements = 0,
    number = 0,
    size = 10,
    numberOfElements = 0,
    onChange,
  } = props;
  return (
    <div className="flex justify-end">
      <AntPagination
        total={totalElements}
        pageSize={size}
        onChange={onChange}
        showQuickJumper
      />
    </div>
  );
};
