import { Table as AntTable, TableProps as AntTableProps } from "antd";
import React from "react";
import { Pagination } from "../Pagination";
import "./Table.css";

export interface TableProps extends AntTableProps<any> {
  showRank?: boolean;
}

export const Table = (props: TableProps) => {
  const { columns, pagination, showRank = false } = props;

  const getColumns = () => {
    if (showRank) {
      return [
        {
          title: "STT",
          key: "index",
          render: (text: any, record: any, index: any) =>
            (1 - 1) * 10 + index + 1,
        },
        ...(columns as any),
      ];
    }
    return columns;
  };

  return (
    <div className="flex flex-col gap-4">
      <div>
        <AntTable {...props} columns={getColumns()} pagination={false} />
      </div>
      <div>
        <Pagination
          totalElements={100}
          number={1}
          size={10}
          numberOfElements={10}
        />
      </div>
    </div>
  );
};
