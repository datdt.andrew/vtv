import clsx from "clsx";
import React, { ReactNode } from "react";
import { Link } from "react-router-dom";

export type TwMainColor =
  | "yellow"
  | "blue"
  | "green"
  | "gray"
  | "pink"
  | "red"
  | "indigo"
  | "purple";

export interface BadgeProps {
  className?: string;
  text: ReactNode;
  color?: TwMainColor | string;
  bg?: string;
  href?: string;
}

const settings = [
  {
    key: "pink",
    cls: "text-pink-800 bg-pink-100",
    hoverCls: "hover:bg-pink-800",
  },
  {
    key: "red",
    cls: "text-red-800 bg-red-100",
    hoverCls: "hover:bg-red-800",
  },
  {
    key: "gray",
    cls: "text-[#404040] bg-[#F2F2F2]",
    hoverCls: "hover:bg-gray-500",
  },
  {
    key: "green",
    cls: "text-[#4EC260] bg-[#E9F8F5]",
    hoverCls: "hover:bg-green-500",
  },
  {
    key: "purple",
    cls: "text-purple-800 bg-purple-100",
    hoverCls: "hover:bg-purple-800",
  },
  {
    key: "indigo",
    cls: "text-indigo-800 bg-indigo-100",
    hoverCls: "hover:bg-indigo-800",
  },
  {
    key: "yellow",
    cls: "text-[#FFB916] bg-[#FFF9E3]",
    hoverCls: "hover:bg-yellow-500",
  },
  {
    key: "blue",
    cls: "text-[#2563EB] bg-[#BFDBFE]",
    hoverCls: "hover:bg-blue-500",
  },
  {
    key: "blue",
    cls: "text-[#2563EB] bg-[#BFDBFE]",
    hoverCls: "hover:bg-blue-500",
  },
];
export const Badge: React.FC<BadgeProps> = ({
  className = "relative",
  text,
  color = "gray",
  bg = "",
  href,
}) => {
  const getColorClass = () => {
    const setting = settings.find((x: any) => x.key === color);
    if (!setting) return null;
    return setting;
  };
  const cls = getColorClass();

  const customStyles = !cls
    ? {
        color: color,
        backgroundColor: bg,
      }
    : {};

  const CLASSES =
    "inline-flex px-2.5 py-1 rounded-md font-medium text-[14px] " + className;

  return !!href ? (
    <Link
      to={href || ""}
      className={clsx(
        `transition-colors hover:text-white duration-300 ${CLASSES} ${cls?.cls} ${cls?.hoverCls}`
      )}
      style={customStyles}
    >
      {text}
    </Link>
  ) : (
    <span
      className={`${CLASSES} ${cls?.cls} ${className} `}
      style={customStyles}
    >
      {text}
    </span>
  );
};
