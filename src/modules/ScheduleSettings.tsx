import { DatePicker, Divider, Input, Radio, Select } from "antd";
import React, { useState } from "react";
import { DayInMonthSelect } from "./DayInMonthSelect";
import { WeekDaysPick } from "./WeekDaysPick";

export const ScheduleSettings = (props: any) => {
  const [value, onChange] = useState<any>(null);
  const [repeatType, setRepeatType] = useState<any>();
  const getRepeatTypeRender = () => {
    if (repeatType === "day") return null;
    if (repeatType === "week") {
      return (
        <div className="grid grid-cols-5 items-center">
          <div>On</div>
          <div className="col-span-4">
            <WeekDaysPick />
          </div>
        </div>
      );
    }
    if (repeatType === "month") {
      return (
        <>
          <div className="grid grid-cols-5 items-center">
            <div>Vào</div>
            <div className="col-span-4">
              <DayInMonthSelect />
            </div>
          </div>
          <div className="grid grid-cols-5 items-center">
            <div>Lúc</div>
            <div className="col-span-4">
              <DatePicker size="large" />
            </div>
          </div>
        </>
      );
    }
  };

  return (
    <div className="flex flex-col gap-4">
      <div
        className="flex gap-1 items-baseline"
        onClick={() => onChange("single")}
      >
        <Radio checked={value == "single"} />
        <div className="flex flex-col">
          <span className="font-semibold">MỘT LẦN</span>
          <p>Chỉ gửi tin nhắn một lần tại thời điểm chạy</p>
          <DatePicker />
        </div>
      </div>
      <div
        className="flex gap-1 items-baseline"
        onClick={() => onChange("schedule")}
      >
        <Radio checked={value == "schedule"} />
        <div className="flex flex-col">
          <span className="font-semibold">ĐẶT LỊCH</span>
          <p>Chạy nhiều lần vào thời gian đặt lịch </p>
        </div>
      </div>
      {value === "schedule" && (
        <div className="bg-gray-100 rounded px-8 py-5">
          <div className="flex flex-col gap-4">
            <div className="grid grid-cols-5 items-center">
              <div>Lặp lại mỗi</div>
              <div className="col-span-4">
                <div className="flex items-center gap-2">
                  <Input className="w-24" />
                  <Select
                    className="w-32"
                    size="large"
                    defaultValue={"day"}
                    onChange={setRepeatType}
                    value={repeatType}
                    options={[
                      {
                        label: "Ngày",
                        value: "day",
                      },
                      {
                        label: "Tuần",
                        value: "week",
                      },
                      {
                        label: "Tháng",
                        value: "month",
                      },
                    ]}
                  />
                  <div className="flex gap-2 items-center">
                    <span>Lúc</span>
                    <DatePicker size="large" />
                  </div>
                </div>
              </div>
            </div>
            {getRepeatTypeRender()}
          </div>

          <Divider dashed />
          <div className="flex flex-col gap-4">
            <div className="grid grid-cols-5 items-center">
              <div>Bắt đầu</div>
              <div className="col-span-4">
                <DatePicker
                  size="large"
                  className="w-full"
                  placeholder="Chọn ngày và thời gian"
                />
              </div>
            </div>
            <div className="grid grid-cols-5 items-center">
              <div>Kết thúc</div>
              <div className="col-span-4">
                <Select
                  size="large"
                  className="w-full"
                  placeholder="Chọn lựa kết thúc"
                  options={[
                    {
                      label: "Vào",
                      value: "in",
                    },
                    {
                      label: "Sau",
                      value: "afet",
                    },
                    {
                      label: "Không",
                      value: "no",
                    },
                  ]}
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
