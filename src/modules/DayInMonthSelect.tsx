import { Select } from "antd";
import React from "react";

export const DayInMonthSelect = () => {
  const daysInMonth = Array.from({ length: 31 }, (_, index) => ({
    value: (index + 1).toString(),
    label: (index + 1).toString(),
  }));

  return (
    <>
      <Select size="large" mode="multiple" options={daysInMonth} />
    </>
  );
};
