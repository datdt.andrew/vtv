import { Button } from "antd";
import React, { useState } from "react";

const days: any = [
  {
    label: "Mon",
    value: "mon",
  },
  {
    label: "Tue",
    value: "tue",
  },
  {
    label: "Wed",
    value: "wed",
  },
  {
    label: "Thu",
    value: "thu",
  },
  {
    label: "Fri",
    value: "fri",
  },
  {
    label: "Sat",
    value: "sat",
  },
  {
    label: "Sun",
    value: "sun",
  },
];

export const WeekDaysPick = () => {
  const [selecteds, setSelecteds] = useState<any>([]);

  const onSelect = (val: any) => {
    const isSelected = selecteds.includes(val);
    if (isSelected) {
      setSelecteds(selecteds.filter((x: any) => x != val));
    } else {
      setSelecteds([...selecteds, val]);
    }
  };
  return (
    <>
      <div className="grid grid-cols-7 gap-2">
        {days.map((x: any) => (
          <Button
            onClick={() => onSelect(x.value)}
            type={selecteds.includes(x.value) ? "primary" : "default"}
          >
            {x.label}
          </Button>
        ))}
      </div>
    </>
  );
};
