import { Filter, Plus, Search } from "lucide-react";
import { default as React, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, DatePicker, Modal } from "../components/core";
import { Input } from "../components/core/Input";
import { Radio, Space } from "antd";

export const CampaignListFilter = (props: any) => {
  const navigate = useNavigate();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <div className="intro-y flex flex-col sm:flex-row items-center">
        <div className="mr-auto flex items-center gap-4 relative">
          <Input
            prefix={<Search size={16} />}
            placeholder="Tìm kiếm theo tên chiến dịch, Mã chiến dịch"
            size="middle"
          />
          <DatePicker size="middle" />
          <Button icon={<Filter size={16} />}>Lọc</Button>
          <Button icon={<Search size={16} />}>Tìm kiếm</Button>
        </div>
        <div className="w-full sm:w-auto flex mt-4 sm:mt-0 gap-4">
          <Modal
            title="Xuất báo cáo chiến dịch"
            open={isModalOpen}
            onOk={handleOk}
            onCancel={handleCancel}
            footer={
              <div className="flex justify-end items-center gap-2">
                <Button>Huỷ</Button>
                <Button type="primary">Xuất báo cáo</Button>
              </div>
            }
          >
            <div className="flex flex-col gap-2">
              <span className="font-semibold">Các tuỳ chọn xuất</span>
              <div>
                <Radio.Group>
                  <Space direction="vertical">
                    <Radio value={1}>Xuất tất cả các cột</Radio>
                    <Radio value={2}>Xuất (các) cột đã chọn</Radio>
                  </Space>
                </Radio.Group>
              </div>
            </div>
          </Modal>
          <Button onClick={() => setIsModalOpen(true)} icon={<Plus />}>
            Xuất báo cáo
          </Button>
          <Button
            onClick={() => navigate("/create")}
            type="primary"
            icon={<Plus />}
          >
            Tạo mới
          </Button>
        </div>
        <div
          id="creat-permission"
          className="modal modal-slide-over"
          data-tw-backdrop="static"
        >
          <div className="modal-dialog modal-xl">
            <div className="modal-content fix-modal">
              <div className="modal-header">
                <h2 className="font-medium text-lg mr-auto">Tạo nhóm quyền</h2>
                <a data-tw-dismiss="modal" href="javascript:;">
                  <i data-lucide="x" className="w-6 h-6 text-slate-400"></i>
                </a>
              </div>
              <div className="modal-body">
                <div>
                  <label className="form-label">Tên nhóm quyền</label>
                  <input
                    id="regular-form-1"
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên nhóm quyền"
                  />
                </div>

                <div className="mt-3">
                  <label className="form-label">Mô tả</label>
                  <input
                    id="regular-form-2"
                    type="text"
                    className="form-control"
                    placeholder="Nhập mô tả"
                  />
                </div>
                <div className="intro-y items-center mt-5">
                  <h2 className="font-medium mr-auto">Phân quyền</h2>
                  <p>
                    Thiết lập nhóm người dùng và quyền trên từng module của hệ
                    thống
                  </p>
                </div>
                <table width="100%" className="v-permission-table mt-5">
                  <tr>
                    <th>Modules</th>
                    <th>Quyền</th>
                  </tr>
                  <tr>
                    <td>
                      <div id="faq-accordion-1" className="accordion">
                        <div className="accordion-item ">
                          <div
                            id="faq-accordion-content-1"
                            className="accordion-header"
                          >
                            <button
                              className="accordion-button"
                              type="button"
                              data-tw-toggle="collapse"
                              data-tw-target="#faq-accordion-collapse-1"
                              aria-expanded="true"
                              aria-controls="faq-accordion-collapse-1"
                            >
                              Campaign
                            </button>
                          </div>
                          <div
                            id="faq-accordion-collapse-1"
                            className="accordion-collapse collapse show"
                            aria-labelledby="faq-accordion-content-1"
                            data-tw-parent="#faq-accordion-1"
                          >
                            <div className="accordion-body text-slate-600 dark:text-slate-500 leading-relaxed">
                              Message
                            </div>
                          </div>
                        </div>
                        <div className="accordion-item">
                          <div
                            id="faq-accordion-content-2"
                            className="accordion-header"
                          >
                            <button
                              className="accordion-button collapsed"
                              type="button"
                              data-tw-toggle="collapse"
                              data-tw-target="#faq-accordion-collapse-2"
                              aria-expanded="false"
                              aria-controls="faq-accordion-collapse-2"
                            >
                              Audiences
                            </button>
                          </div>
                          <div
                            id="faq-accordion-collapse-2"
                            className="accordion-collapse collapse"
                            aria-labelledby="faq-accordion-content-2"
                            data-tw-parent="#faq-accordion-1"
                          >
                            <div className="accordion-body text-slate-600 dark:text-slate-500 leading-relaxed">
                              Lorem Ipsum is simply dummy text of the printing
                              and typesetting industry.
                            </div>
                          </div>
                        </div>
                        <div className="accordion-item">
                          <div
                            id="faq-accordion-content-3"
                            className="accordion-header"
                          >
                            <button
                              className="accordion-button collapsed"
                              type="button"
                              data-tw-toggle="collapse"
                              data-tw-target="#faq-accordion-collapse-3"
                              aria-expanded="false"
                              aria-controls="faq-accordion-collapse-3"
                            >
                              Calendar
                            </button>
                          </div>
                          <div
                            id="faq-accordion-collapse-3"
                            className="accordion-collapse collapse"
                            aria-labelledby="faq-accordion-content-3"
                            data-tw-parent="#faq-accordion-1"
                          >
                            <div className="accordion-body text-slate-600 dark:text-slate-500 leading-relaxed">
                              Lorem Ipsum is simply dummy
                            </div>
                          </div>
                        </div>
                        <div className="accordion-item">
                          <div
                            id="faq-accordion-content-4"
                            className="accordion-header"
                          >
                            <button
                              className="accordion-button collapsed"
                              type="button"
                              data-tw-toggle="collapse"
                              data-tw-target="#faq-accordion-collapse-4"
                              aria-expanded="false"
                              aria-controls="faq-accordion-collapse-4"
                            >
                              Report
                            </button>
                          </div>
                          <div
                            id="faq-accordion-collapse-4"
                            className="accordion-collapse collapse"
                            aria-labelledby="faq-accordion-content-4"
                            data-tw-parent="#faq-accordion-1"
                          >
                            <div className="accordion-body text-slate-600 dark:text-slate-500 leading-relaxed">
                              Lorem Ipsum is simply dummy text of the printing
                              and typesetting industry. Lorem Ipsum has been the
                              industry's standard dummy text ever since the
                              1500s, when an unknown printer took a galley of
                              type and scrambled it to make a type specimen
                              book. It has survived not only five centuries, but
                              also the leap into electronic typesetting,
                              remaining essentially unchanged.
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <div className="v-permission-radio ">
                        <label className="v-permission-lbl">
                          Tùy chỉnh quyền truy cập vào chiến dịch hiển thị
                        </label>
                        <div className="form-check mt-2">
                          <input
                            id="checkbox-switch-1"
                            className="form-check-input"
                            type="checkbox"
                            value=""
                          />
                          <label className="form-check-label">Tạo mới</label>
                        </div>
                        <div className="form-check mt-2">
                          <input
                            id="checkbox-switch-2"
                            className="form-check-input"
                            type="checkbox"
                            value=""
                          />
                          <label className="form-check-label">Sửa</label>
                        </div>
                        <div className="form-check mt-2">
                          <input
                            id="checkbox-switch-3"
                            className="form-check-input"
                            type="checkbox"
                            value=""
                          />
                          <label className="form-check-label">
                            Xem quản lý loại chiến dịch nội bộ
                          </label>
                        </div>
                        <div className="form-check mt-2">
                          <input
                            id="checkbox-switch-4"
                            className="form-check-input"
                            type="checkbox"
                            value=""
                          />
                          <label className="form-check-label">Xóa</label>
                        </div>
                        <div className="form-check mt-2">
                          <input
                            id="checkbox-switch-5"
                            className="form-check-input"
                            type="checkbox"
                            value=""
                          />
                          <label className="form-check-label">Export</label>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div className="modal-footer w-full absolute bottom-0">
                <button
                  type="button"
                  data-tw-dismiss="modal"
                  className="btn btn-outline-secondary w-20 mr-1"
                >
                  Hủy
                </button>
                <button type="submit" className="btn btn-primary w-20">
                  Tạo
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
