import { BrowserRouter, Route, Routes } from "react-router-dom";

import React from "react";
import "./App.css";
import { DefaultLayout } from "./components/layouts/DefaultLayout";
import { CreateCampaign } from "./pages/CreateCampaign";
import { ListCampaign } from "./pages/ListCampaign";
import { ViewCampaign } from "./pages/ViewCampaign";

function App() {
  return (
    <>
      <BrowserRouter>
        <DefaultLayout>
          <Routes>
            <Route path="/" element={<ListCampaign />} />
            <Route path="/create" element={<CreateCampaign />} />
            <Route path="/view" element={<ViewCampaign />} />
          </Routes>
        </DefaultLayout>
      </BrowserRouter>
    </>
  );
}

export default App;
