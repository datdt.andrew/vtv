import React from "react";
import { Collapse } from "../components/core";
import "./styles.css";
const PropertyRow = (props: any) => {
  const { label, value } = props;
  return (
    <div className="detail-table-view__row">
      <div className="col-span-3">{label}</div>
      <div className="col-span-9">{value}</div>
    </div>
  );
};

const PropertyView = (props: any) => {
  const { dataSource } = props;
  return (
    <>
      <div className="detail-table-view">
        {dataSource.map((x: any) => (
          <PropertyRow {...x} />
        ))}
      </div>
    </>
  );
};

const general = [
  {
    label: "Campaign name",
    value: "ducnt19419_193891",
  },
  {
    label: "Status",
    value: "Running",
  },
  {
    label: "Chanel",
    value: "SMS",
  },
  {
    label: "SMS Type",
    value: "Promotion",
  },
  {
    label: "Business Unit",
    value: "VTV Live",
  },
  {
    label: "Brand Name",
    value: "VTV",
  },
  {
    label: "Campaign Goals",
    value: "PREPAID, INSURANCE, Linked account",
  },
  {
    label: "Delivery Type",
    value: "Schedule",
  },
  {
    label: "Start sending on",
    value: "01/07/2023 00:00:00",
  },
  {
    label: "End on",
    value: "01/10/2023 00:00:00",
  },
  {
    label: "When user triggers",
    value: "new_purchase_100",
  },
];

const targeting = [
  {
    label: "Target",
    value: "N/A",
  },
  {
    label: "Audience included",
    value: "N/A",
  },
  {
    label: "Audience narrowed",
    value: "N/A",
  },
  {
    label: "Further narrowed",
    value: "N/A",
  },
];
export const ViewCampaign = (props: any) => {
  const { Panel } = Collapse;

  return (
    <div className="mt-5">
      <Collapse defaultActiveKey={["1"]}>
        <Panel header="Thông tin chung" key="1">
          <PropertyView dataSource={general} />
        </Panel>
        <Panel header="Targeting" key="2">
          <PropertyView dataSource={targeting} />
        </Panel>
        <Panel header="Content" key="3"></Panel>
      </Collapse>
    </div>
  );
};
