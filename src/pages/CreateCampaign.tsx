import { Alert, Button, Card, Input, Steps } from "antd";
import { Gift, Headphones } from "lucide-react";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Select } from "../components/core/Select";
import { FormBuilder } from "../components/shared/FormBuilder/FormBuilder";
import { FormBuilderOption } from "../components/shared/FormBuilder/types";
import { ScheduleSettings } from "../modules/ScheduleSettings";
import { useNavigate } from "react-router-dom";
export const CreateCampaign = (props: any) => {
  const [step, setStep] = useState(0);
  const navigation = useNavigate();
  const {
    control,
    handleSubmit,
    watch,
    getValues,
    setValue,
    trigger,
    formState: { errors },
  } = useForm({
    defaultValues: {} as any,
  });

  const generalInformationItems: FormBuilderOption[] = [
    {
      cols: 1,
      items: [
        {
          name: `campaignName`,
          label: "Tên chiến dịch",
          required: true,
          component: Input,
          customProps: {
            size: "large",
            placeholder: "Nhập tên chiến dịch",
          },
        },
      ],
    },
    {
      cols: 1,
      items: [
        {
          name: `createdDate`,
          label: "Loại SMS",
          component: (e: any) => (
            <div className="flex gap-4">
              <div className="bg-green-300 flex border-green-700 rounded items-center gap-2 w-40 p-4 font-semibold border">
                <Gift /> Quảng cáo
              </div>
              <div className="p-4 w-40 border flex items-center rounded gap-2">
                <Headphones />
                CSKH
              </div>
            </div>
          ),
          customProps: {},
        },
      ],
    },
    {
      cols: 1,
      items: [
        {
          name: `branchName`,
          label: "Branch name",
          required: true,
          component: Input,
          customProps: {
            size: "large",
            placeholder: "Nhập tên branch",
          },
        },
      ],
    },
    {
      cols: 1,
      items: [
        {
          name: `branchName`,
          label: "Mục tiêu chiến dịch",
          required: true,
          component: Select,
          customProps: {
            size: "large",
            placeholder: "Nhập tên branch",
          },
        },
      ],
    },
  ];

  const customerItems: FormBuilderOption[] = [
    {
      cols: 1,
      items: [
        {
          name: `customerFile`,
          label: "Chọn một tập khách hàng đã được lọc và tạo trên Metabase.",
          component: Select,
          customProps: {
            size: "large",
            placeholder: "Chọn tệp khách hàng",
          },
        },
      ],
    },
  ];

  const contentItems: FormBuilderOption[] = [
    {
      cols: 1,
      items: [
        {
          name: `customerFile`,
          label: "Mẫu SMS",
          component: Select,
          customProps: {
            size: "large",
            placeholder: "Chọn mẫu SMS",
          },
        },
      ],
    },
    {
      cols: 1,
      items: [
        {
          name: `customerFile`,
          label: "Nội dung SMS",
          required: true,
          component: Input,
          customProps: {
            size: "large",
            placeholder: "Chọn mẫu SMS",
          },
        },
      ],
    },
  ];

  return (
    <>
      <Card title="Tạo chiến dịch gửi tin nhắn" className="mt-5">
        <Steps
          current={step}
          items={[
            {
              title: "THÔNG TIN CHUNG",
            },
            {
              title: "TỆP KHÁCH HÀNG",
            },
            {
              title: "NỘI DUNG",
            },
          ]}
        />
      </Card>

      {step == 0 && (
        <div className="grid grid-cols-12 gap-6">
          <div className="intro-y col-span-12 lg:col-span-12">
            <div className="intro-y box mt-5 p-5 flex flex-col gap-5">
              <div className="intro-y flex flex-col">
                <div className="mb-4">THÔNG TIN CHUNG</div>
                <div className="flex flex-col gap-5">
                  <FormBuilder
                    itemOptions={generalInformationItems}
                    control={control}
                    errors={errors}
                  />
                </div>
              </div>
            </div>
            <div className="intro-y box mt-5 p-5 flex flex-col gap-5">
              <div className="intro-y flex flex-col">
                <div className="mb-4">LỊCH CHẠY</div>
                <ScheduleSettings />
              </div>
            </div>
          </div>
        </div>
      )}

      {step == 1 && (
        <div className="grid grid-cols-12 gap-6">
          <div className="col-span-8">
            <div className="intro-y col-span-12 lg:col-span-12">
              <div className="intro-y box mt-5 p-5 flex flex-col gap-5">
                <div className="intro-y flex flex-col">
                  <div className="mb-4">TỆP KHÁCH HÀNG</div>
                  <div className="flex flex-col gap-5">
                    <FormBuilder
                      itemOptions={customerItems}
                      control={control}
                      errors={errors}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-4">
            <div className="intro-y col-span-12 lg:col-span-12">
              <div className="intro-y box mt-5 p-5 flex flex-col gap-5">
                <div className="intro-y flex flex-col">
                  <div className="mb-4">SỐ LƯỢNG ĐỐI TƯỢNG QUẢNG CÁO</div>
                  <div className="flex flex-col gap-5">
                    <div className="p-4 flex items-center justify-center rounded bg-gray-300">
                      ------
                    </div>
                    <Alert
                      message="Thông báo"
                      description="Bằng cách xuất bản chiến dịch mới, đối tượng hỗn hợp mới sẽ được tạo tự động và có thể được quản lý thông qua danh sách đối tượng hỗn hợp."
                      type="success"
                      showIcon
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      {step == 2 && (
        <div className="grid grid-cols-12 gap-6">
          <div className="col-span-8">
            <div className="intro-y col-span-12 lg:col-span-12">
              <div className="intro-y box mt-5 p-5 flex flex-col gap-5">
                <div className="intro-y flex flex-col">
                  <div className="mb-4">TỆP KHÁCH HÀNG</div>
                  <div className="flex flex-col gap-5">
                    <FormBuilder
                      itemOptions={contentItems}
                      control={control}
                      errors={errors}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-4">
            <div className="intro-y col-span-12 lg:col-span-12">
              <div className="intro-y box mt-5 p-5 flex flex-col gap-5">
                <div className="intro-y flex flex-col">
                  <div className="mb-4">XEM TRƯỚC NỘI DUNG</div>
                  <div className="flex flex-col gap-5">
                    <div className="p-4 flex items-center justify-center rounded bg-gray-300 gap-1">
                      <Button>IOS</Button>
                      <Button>Android</Button>
                    </div>
                    <Alert
                      description="Xem trước nội dung có thể không hoàn toàn giống như bạn thấy trên ứng dụng."
                      type="info"
                      showIcon
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      <Card className="mt-5">
        <div className="flex justify-between gap-4 items-center">
          <Button onClick={() => navigation("/")}>Huỷ</Button>
          <div className="flex items-center gap-4">
            <Button>Lưu Nháp</Button>
            <Button onClick={() => setStep(step - 1)}>Quay lại</Button>
            <Button type="primary" onClick={() => setStep(step + 1)}>
              Tiếp theo
            </Button>
          </div>
        </div>
      </Card>
    </>
  );
};
