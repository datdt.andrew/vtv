import { ExclamationCircleFilled } from "@ant-design/icons";
import { MenuProps, Modal } from "antd";
import {
  Check,
  Eye,
  MoreHorizontal,
  Pencil,
  SendIcon,
  Trash,
  X,
} from "lucide-react";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Badge, Button, Dropdown, Table, Tabs } from "../components/core";
import { HeadTitle } from "../components/layouts/Breadcrumb";
import { CampaignListFilter } from "../modules/CampaignListFilter";
import { InputWrapper } from "../components/shared/InputWrapper";
import TextArea from "antd/es/input/TextArea";

export const ListCampaign = (props: any) => {
  const router = useNavigate();
  const { confirm } = Modal;

  const [isOpenApproveModal, setIsOpenApprovalModal] = useState(false);
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {},
    getCheckboxProps: (record: any) => ({
      disabled: record.name === "Disabled User",
      name: record.name,
    }),
  };

  const items: MenuProps["items"] = [
    {
      label: "Xem chi tiết",
      key: "1",
      icon: <Eye size={16} />,
      onClick: () => router("/view"),
    },
    {
      label: "Duyệt",
      key: "2",
      icon: <Check size={16} />,
      onClick: () => {
        confirm({
          title: "Phê duyệt chiến dịch?",
          content: (
            <div className="flex flex-col gap-2">
              <div>Nội dung phê duyệt</div>
              <div>
                <TextArea />
              </div>
            </div>
          ),
        });
      },
    },
    {
      label: "Từ chối",
      key: "3",
      icon: <X size={16} />,
      onClick: () => {
        confirm({
          okButtonProps: {
            style: {
              background: "red",
            },
          },
          okText: "Từ chối",
          title: "Từ chối chiến dịch?",
          content: (
            <div className="flex flex-col gap-2">
              <div>Nội dung từ chối</div>
              <div>
                <TextArea />
              </div>
            </div>
          ),
        });
      },
    },
    {
      label: "Sửa",
      key: "4",
      icon: <Pencil size={16} />,
    },
    {
      label: "Gửi mẫu",
      key: "5",
      icon: <SendIcon size={16} />,
    },
    {
      label: "Xoá",
      key: "6",
      icon: <Trash size={16} />,
      danger: true,
    },
  ];

  const menuProps = {
    items,
    onClick: () => {},
  };

  const columns = [
    {
      dataIndex: "code",
      title: "Mã chiến dịch",
      sorter: (a: any, b: any) => a.name.length - b.name.length,
    },
    {
      dataIndex: "name",
      title: "Tên chiến dịch",
      sorter: (a: any, b: any) => a.name.length - b.name.length,
    },
    {
      dataIndex: "createdDate",
      title: "Ngày tạo",
      sorter: (a: any, b: any) => a.name.length - b.name.length,
    },
    {
      dataIndex: "createdBy",
      title: "Người tạo",
      sorter: (a: any, b: any) => a.name.length - b.name.length,
    },
    {
      dataIndex: "status",
      title: "Trạng thái",
      render: (e: any) => <>{getBadgeRender(e)}</>,
    },
    {
      dataIndex: "action",
      title: "Hành động",
      render: (e: any, record: any) => {
        return (
          <>
            <Dropdown menu={{ items }}>
              <Button>
                <MoreHorizontal size={16} />
              </Button>
            </Dropdown>
          </>
        );
      },
    },
  ];

  const getBadgeRender = (status: any) => {
    switch (status) {
      case "RUNNING":
        return <Badge text="Đang chạy" color="green" />;
      case "ACCEPTED":
        return <Badge text="Đã duyệt" color="blue" />;
      case "CANCELED":
        return <Badge text="Huỷ" color="yellow" />;
      case "ENDED":
        return <Badge text="Huỷ" />;
    }
    return <Badge text={status} />;
  };

  const mockData = [
    {
      code: 32258,
      name: "DDS",
      createdDate: "2023-09-24T12:28:00.480Z",
      createdBy: "Asa.Moore",
      id: "1",
      status: "RUNNING",
    },
    {
      code: 24472,
      name: "II",
      createdDate: "2023-09-24T16:16:39.253Z",
      createdBy: "Annamae67",
      id: "2",
      status: "ACCEPTED",
    },
    {
      code: 77556,
      name: "DVM",
      createdDate: "2023-09-24T11:47:18.936Z",
      createdBy: "Walker_Herzog",
      id: "3",
      status: "ENDED",
    },
    {
      code: 64024,
      name: "DDS",
      createdDate: "2023-09-25T05:21:53.993Z",
      createdBy: "Francesca.Keeling11",
      id: "4",
      status: "CANCELED",
    },
  ];

  const onCloseApprovalModal = () => {
    setIsOpenApprovalModal(false);
  };

  return (
    <>
      <HeadTitle
        title="Phân quyền"
        description="Thiết lập nhóm người dùng và quyền trên từng module của hệ thống"
      />
      <div className="grid grid-cols-12 gap-6">
        <div className="intro-y col-span-12 lg:col-span-12">
          <div className="intro-y box mt-5 p-5 flex flex-col gap-5">
            <div className="intro-y flex flex-col sm:flex-row items-center">
              <Tabs className="w-full">
                <Tabs.TabPane
                  tab={
                    <>
                      Duyệt <Badge text="99" color="green" />
                    </>
                  }
                  key="approved"
                >
                  <div className="flex flex-col gap-5">
                    <CampaignListFilter />
                    <div>
                      <Table
                        dataSource={mockData}
                        columns={columns}
                        rowSelection={rowSelection}
                      />
                    </div>
                  </div>
                </Tabs.TabPane>
                <Tabs.TabPane
                  tab={
                    <>
                      Chờ duyệt <Badge text="50" />
                    </>
                  }
                  key="waiting"
                >
                  <div className="flex flex-col gap-5">
                    <CampaignListFilter />
                    <div>
                      <Table
                        dataSource={mockData}
                        columns={columns}
                        rowSelection={rowSelection}
                      />
                    </div>
                  </div>
                </Tabs.TabPane>
              </Tabs>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
